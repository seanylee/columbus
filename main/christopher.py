#-*- coding:utf-8 -*-

import sys, os # Command Line Arguments 를 받는데에만 사용! | path 추출하는데 사용
import random
import string
import psutil

from datetime import datetime
from pytz import timezone

from subprocess import check_output
import subprocess
import argparse

from jobposition.tools import config, awsutils, utils, comment




class Ship:
    """
    Ship 클래스에서 업무 할당 (job_assign), 위치 할당 (position_assign), 시간 할당 (time_scheduler)
    최종적으로 선원 모집 ==> recruiting
    """
    
    def __init__(self, _load_cmd, _collect_cmd, _render_cmd, _schedule_cmd):
        """
        허용되는 Arguments 항목 사전 정의
        """
        self.__ACCEPT_MAP       = {
                                    'load'    :_load_cmd,
                                    'collect' :_collect_cmd,
                                    'render'  :_render_cmd,
                                    'schedule':_schedule_cmd
                                  }

        self.__ACCEPT_JOBS      = list(self.__ACCEPT_MAP.keys())
        self.__ACCEPT_POSITIONS = [(value) for list_value in self.__ACCEPT_MAP.values() for value in list_value]
        self.__ACCEPT_TIMES     = list(self.__ACCEPT_MAP['schedule'])
        
    def identity_check(self, _id_job, _id_position, _id_time):
        if _id_job in self.__ACCEPT_JOBS:
            possible_position =self.__ACCEPT_MAP[_id_job]
            if _id_position in possible_position: 
                if _id_time in self.__ACCEPT_TIMES:
                    return (0, _id_job, _id_position, _id_time)
                else:
                    return (1, '"{}" is not a valid argument for schedule'.format(_id_time))
            else:
                return (1, '"{}" is not a valid argument for position'.format(_id_position))
        else:
            return (1, '"{}" is not a valid argument for job'.format(_id_job))


    def recruiting(self, _job, _position, _time):
        """
        목적 : load, collect, render 의 종속관계 정의 해주기.
        """
        id_check = self.identity_check(_job, _position, _time)
        if id_check[0]==0:
            valid_job      = id_check[1]
            valid_position = id_check[2]
            valid_time     = id_check[3]
        else:
            print(id_check[1])
            return id_check[1]


        if valid_job == 'load':
            file_path_job = 'acquired'
        elif valid_job == 'collect':
            file_path_job = 'collection'
        elif valid_job == 'render':
            file_path_job = 'render'
        elif valid_job == 'schedule':
            file_path_job = 'schedule'
        else:
            file_path_job = ''
        
        if valid_position == 'public':
            file_path_position = '_public_loader'
        elif valid_position == 'private':
            file_path_position = '_private_loader'
        elif valid_position == 'yourself':
            file_path_position = '_yourself_loader'
        elif valid_position in ['producer','consumer']:
            file_path_position = '_' + valid_position
        else:
            file_path_position=''

        today = utils.date_point(_tense='present', _output='date_val')[1]
        exec_path = os.getcwd() + '/jobposition/' + file_path_job + file_path_position + '.py'
        logs_path = os.getcwd() + '/log/' + today + '_' + file_path_job + file_path_position + '.log'

        try:
            command = (
                "nohup python3 -u" 
                + " " + exec_path          # 실행파일
                + " " + valid_job          # Argument 입력 영역
                + " " + valid_position            
                + " " + valid_time
                + " >> " + logs_path)       # Log 수집 영역 (Append)
            print(command)
            proc = subprocess.Popen(command,shell=True)
            print("start 'consumer' process with pid {}".format(proc.pid))
            print("In order to kill the background process : 'kill -9 {}'".format(proc.pid))
        except:
            print('command failed to assembled')



# Argument Parser를 사용해서 효율적인 Argument 입력 도움 제공!
datetime_val = utils.date_point(_tense='present', _output='datetime_val')[1]
parser = argparse.ArgumentParser(description=(
	(comment.HELP_MAIN.format(date=datetime_val)).replace('\t','')),formatter_class=argparse.RawTextHelpFormatter)
                                    

group = parser.add_mutually_exclusive_group()
group.add_argument('-v', '--version', action='store_true', help='show currently running version of Christopher')
group.add_argument('-m', '--migration', action='store_true', help='model migration')
#group.add_argument('-c', '--christopher', action='store_true', help='Christopher project procedure')
#group.add_argument('-p', '--purge_resource', action='store_true', help='wipeout whole resources on SQS or S3')
#group.add_argument('-s', '--sqs_message', action='store_true', help='retrieve active queues')
#group.add_argument('-r', '--rendered_s3', action='store_true', help='retrieve rendered html file list from s3')
#group.add_argument('-t', '--top_resource', action='store_true', help='check currently running resources: cpu, mem, pid')

job_list        = str(list(config.ACCEPTED_COMMAND.keys()))
position_list   = str([(value) for list_value in config.ACCEPTED_COMMAND.values() for value in list_value])
time_list       = str(config.ACCEPTED_COMMAND['schedule'])

parser.add_argument('-j','--job', type=str, metavar='', required=False, 
                    help=': define sailor\'s job => {}'.format(job_list))

parser.add_argument('-p','--position', type=str, metavar='', required=False, 
                    help=': define sailor\'s position => {}'.format(position_list))

parser.add_argument('-t','--time', type=str, metavar='', required=False, 
                    help=': define sailor\'s detail job description => {}'.format(time_list))

args = parser.parse_args()




if __name__ =='__main__':

    if args.migration:
        subprocess.call(["python", "./jobposition/tools/models.py", "migrate"])


    elif args.version:
        print("Christopher v0.0.1 :: Uneedcomms, Inc.")


    elif (args.job and args.position and args.time):
        LOAD        = config.ACCEPTED_COMMAND['load']
        COLLECT     = config.ACCEPTED_COMMAND['collect']
        RENDER      = config.ACCEPTED_COMMAND['render']
        SCHEDULE    = config.ACCEPTED_COMMAND['schedule']

        ship=Ship(LOAD, COLLECT, RENDER, SCHEDULE)
        recruit = ship.recruiting(args.job, args.position, args.time)
        
    else:
        None
