# Christopher Columbus

- readme


# Trouble Shooting

## Initiate Process
1. .py run - models.py migrate
2. .py run - models.py initiate
3. db modify - BusinessDataCollection >> business number >> int4 -> int8 
4. .py run - Jobs should be executed in order of "Load(Private | Public | Yourself) - Collect(Producer | Consuemr) - Render (Producer | Consumer)"
5. .py run - Launch url-load-worker : load url data from business-data-collection to business-data-collection-url 

## Initiate From Running Process
1. All rows and cascade have to be removed before carry the process
2. Drop all tables related to the project
3. Purge whole pending messages on SQS
4. Go back to "Initiate Process" and follow the procedure

