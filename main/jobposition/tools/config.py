#-*- coding:utf-8 -*-
# config.py :: Confidential Resource Configuration


import sys, os
# Set execution base directory to be main 
sys.path.append(os.path.dirname(os.path.abspath(
                os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
                )))


from jobposition.tools import utils




# AWS BOTO3 Configuration
ACCESS_KEY      = 'AKIA5QZ4RKKF2PQLTAMH'
SECRET_KEY      = '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'
REGION_NAME     = 'ap-northeast-2'


# AWS SQS Configuration
SQS_QUEUE_URL = {
    'COLLECT' : 'https://sqs.ap-northeast-2.amazonaws.com/929450513035/christopher_collect_queue.fifo',
    'RENDER'  : 'https://sqs.ap-northeast-2.amazonaws.com/929450513035/christopher_render_queue.fifo',
    'collect' : 'https://sqs.ap-northeast-2.amazonaws.com/929450513035/standard-data-queue',
    'render'  : 'https://sqs.ap-northeast-2.amazonaws.com/929450513035/standard_rendering_queue',
    }


# Default batch max value -> Do not change it!    
SQS_BATCH_MAX = 10


# DB Configuration
POSTGRESQL = {
    'host' : 'lead-database.c46ckajca651.ap-northeast-2.rds.amazonaws.com',
    'name' : 'christopher',
    'user' : 'columbus_crud',
    'pass' : 'ChristopherColumbus3515',
    }


# Slack Notification
SLACK_NOTI_URL = ''


# JobPositionTime Definition
ACCEPTED_COMMAND = {
    'load' : ['public', 'private', 'yourself'],
    'collect' : ['producer', 'consumer'],
    'render' : ['producer', 'consumer'],
    'schedule' : ['now']
    }



# OpenAPI Configuration
OPENAPI_URL = {
    'MllInfoService_seq'     :  ("http://apis.data.go.kr/1130000/MllInfoService/getMllInfoDetail"
                                    +"?seq={}&ServiceKey={}"),
    'MllInfoService_period'  :  ("http://apis.data.go.kr/1130000/MllInfoService/getMllSttemntInfo?"
                               + "&fromPermYmd={dateval}&toPermYmd={dateval}&ServiceKey={}")
    }


OPENAPI_KEY = {
    'data_go_kr' : [
        'N2HrBploz4wlG0wEqGbdJWaQ%2FPpdiJ819jqa4O35o%2FuyMLlmwY8BH9yU4wG8mkbAh%2F1f7kDW1Gfv1oxk70Dh%2BA%3D%3D',
	    '7WQIYmLZczGG2npwM2X7WpyjmB2210WqM9z1O9SSUrwcUovJNPKfWlkIPCSMwPCw97bQeO7Z6vtf7tSUKfk64Q%3D%3D'
        ],
    'data_sould_go_kr' : [
        ''
        ]
	}



MOBILE_AND_UA = {
    'chrome74' : ('Mozilla/5.0 (Linux; Android 9; SM-G960F Build/PPR1.180610.011; wv) '
                + 'AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 '
                + 'Mobile Safari/537.36'),
    'chrome63' : ('Mozilla/5.0 (Linux; Android 6.0.1; SM-G532G Build/MMB29T) '
                + 'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.83 '
                + 'Mobile Safari/537.36')
    }

MOBILE_IOS_UA = {
    'safari12' : ('Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) '
                + 'AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1'),
    'webkit'   : ('Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) '
                + 'AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148')
    }

PC_WINDOW_UA = {
    'chrome74' : ('Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' 
                + 'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),
    'ie11'     : ('Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko')
    }

PC_MAC_UA = {
    'chrome74' : ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) '
                + 'Chrome/74.0.3729.169 Safari/537.36'),
    'webkit'   : ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko)')
    }



if __name__ == "__main__":

    if sys.argv[1] == 'test':
        print(utils.string_align(_formatting_str='AWS RESOURCE',_text_align='center',_expression='-',_gap='50')[1])
        print(utils.string_align(_formatting_str='ACCESS_KEY',_text_align='left',_expression='.',_gap='50')[1] 
                + ('[OK]' if ACCESS_KEY else '[NO]'))
        print(utils.string_align(_formatting_str='SECRET_KEY',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if SECRET_KEY else '[NO]'))
        print(utils.string_align(_formatting_str='REGION_NAME',_text_align='left',_expression='.',_gap='50')[1] 
                + ('[OK]' if REGION_NAME else '[NO]'))
        print(utils.string_align(_formatting_str='SQS_QUEUE_URL',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if SQS_QUEUE_URL.keys() else '[NO]'))

        print(utils.string_align(_formatting_str='POSTGRES RESOURCE',_text_align='center',_expression='-',_gap='50')[1])
        print(utils.string_align(_formatting_str='POSTGRESQL HOST',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if POSTGRESQL['host'] else '[NO]'))
        print(utils.string_align(_formatting_str='POSTGRESQL DB',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if POSTGRESQL['name'] else '[NO]'))
        print(utils.string_align(_formatting_str='POSTGRESQL USER',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if POSTGRESQL['user'] else '[NO]'))
        print(utils.string_align(_formatting_str='POSTGRESQL PASSWORD',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if POSTGRESQL['pass'] else '[NO]'))
    
    if sys.argv[1] == 'arguments':
        print(ACCEPTED_COMMAND['load'])
