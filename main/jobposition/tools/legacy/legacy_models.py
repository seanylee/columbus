import sys
import config
import utils
import sqlalchemy
import tabulate
#from sqlalchemy.ext.declarative import declarative_base # Database mapping declare

from sqlalchemy import Column, Integer, String, Boolean, DateTime # Column elements, to  be mapped to DB model
from sqlalchemy import create_engine
from sqlalchemy import ForeignKey # Relationship define
from sqlalchemy import create_engine # engine creation
from sqlalchemy import MetaData, Table
from sqlalchemy import inspect

from sqlalchemy.orm import sessionmaker,scoped_session # Session manamgement
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.postgresql import JSON, ENUM
from sqlalchemy.sql import func


engine = create_engine(
                        'postgresql://{user}:{password}@{host}:5432/{db}'.format(
                                user        = config.POSTGRESQL['user'],
                                password    = config.POSTGRESQL['pass'],
                                host        = config.POSTGRESQL['host'],
                                db          = config.POSTGRESQL['name']
                            ),
                            client_encoding='utf-8'
                            , echo=True
                        )
schema_name ='columbus'

session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

metadata = MetaData()




AcquiredPublicSource = Table('acquired_public_source', metadata,
	Column('pubs_id', Integer, primary_key=True),
	Column('pubs_sqs_delivered', Boolean, nullable=False, default=False),
	Column('pubs_provide_orgainz', String),
	Column('pubs_type', String),
	Column('pubs_type_detail', String),
	Column('pubs_type_extra', String),
	Column('pubs_seq', Integer, unique=True),
	Column('pubs_gen_type', String),
	Column('pubs_created_at', DateTime(timezone=True), server_default=func.now()),
	schema=schema_name)

AcquiredPrivateSource = Table('acquired_private_source', metadata,
	Column('pris_id', Integer, primary_key=True),
	Column('pris_collection_delivered', Boolean, nullable=False, default=False),
	Column('pris_provide_orgainz', String),
	Column('pris_type', String),
	Column('pris_type_detail', String),
	Column('pris_business_name', String),
	Column('pris_business_num', Integer),
	Column('pris_domain', String),
	Column('pris_created_at', DateTime(timezone=True), server_default=func.now()),
	schema=schema_name)

AcquiredYourselfSource = Table('acquired_yourself_source', metadata,
	Column('yous_id', Integer, primary_key=True),
	Column('yous_collection_delivered', Boolean, default=False),
	Column('yous_provide_organiz', String),
	Column('yous_type', String),
	Column('yous_type_detail', String),
	Column('yous_business_name', String),
	Column('yous_business_num', Integer),
	Column('yous_domain', String),
	Column('yous_created_at', DateTime(timezone=True), server_default=func.now()),
	schema=schema_name)

BusinessDataCollection = Table('business_data_collection', metadata,
	Column('bdc_id', Integer, primary_key=True),
    Column('sqs_consumer_id', Integer, ForeignKey('sqs_consumer.sqs_id')),

	Column('bdc_source_type', String),
	Column('bdc_business_name', String),
	Column('bdc_business_no', Integer, unique=True),
	Column('bdc_url_raw', String),
	Column('bdc_url_scheme', String),
	Column('bdc_url_domain', String, unique=True),
	Column('bdc_url_path', String),
	Column('bdc_raw_message', JSON),
	Column('bdc_created_at', DateTime(timezone=True), server_default=func.now()),
	schema=schema_name)

BusinessDataRendering = Table('business_data_rendering', metadata,
	Column('bdr_id', Integer, primary_key=True),
	Column('bdr_domain', String),
	Column('bdr_origin_html_length', Integer),
	Column('bdr_status_code', Integer),
	Column('bdr_header_raw', String),
	Column('bdr_header_response_time',Integer),
	Column('bdr_body_response_time',Integer),
	Column('bdr_rendering_time',Integer),
	Column('bdr_body_uploaded', Boolean, default=False),
	Column('bdr_body_key', String),
	Column('bdr_created_at', DateTime(timezone=True), server_default=func.now()),
	schema=schema_name)

MappingColumn = Table('mapping_column', metadata,
	Column('col_id', Integer, primary_key=True),
	Column('col_name_en', String),
	Column('col_name_ko', String),
	Column('col_desc', String),
	Column('col_sample_data', String),
	schema=schema_name)

MappingMgmtStatus = Table('mapping_mgmt_status', metadata,
	Column('mgmgt_id', Integer, primary_key=True),
	Column('mgmt_status_code', String),
	Column('mgmt_status_desc', String),
	schema=schema_name)

MappingSellMethod = Table('mapping_sell_method', metadata,
	Column('method_code', String),
	Column('method_desc', String),
	schema=schema_name)

MappingCategory = Table('mapping_category', metadata,
    Column('cate_id', Integer, primary_key=True),
    Column('cate_code', String),
    Column('cate_desc', String),
	schema=schema_name)

SqsConsumer = Table('sqs_consumer', metadata,
    Column('sqs_id', Integer, primary_key=True),
    Column('user_id', Integer, ForeignKey('acquired_public_source.pubs_id')),

    Column('sqs_seq', Integer),
    Column('sqs_job_from', String),
    Column('sqs_consumed', Boolean, default=False),
    Column('sqs_consumed_pid',Integer),
    Column('mq_termin_code', String),
    Column('sqs_consumed_at', DateTime(timezone=True), server_default=func.now()),
    schema=schema_name)

SqsProducer = Table('sqs_producer', metadata,
    Column('sqs_id', Integer, primary_key=True),
    Column('sqs_producer_id', Integer, ForeignKey('sqs_consumer.sqs_id')),

    Column('sqs_seq', Integer),
    Column('sqs_job_from', String),
    Column('sqs_produced', Boolean, default=False),
    Column('sqs_produced_pid',Integer),
    Column('mq_termin_code', String),
    Column('sqs_produced_at', DateTime(timezone=True), server_default=func.now()),
    schema=schema_name)

SqsTerminationRef = Table('sqs_termination_ref', metadata,
    Column('mq_id', Integer, primary_key=True),
    Column('mq_termin_code', String),
    Column('mq_termin_num', Integer),
    Column('mq_termin_letter', String),
    Column('mq_termin_letter_detail', String),
    Column('mq_termin_detail', String),
    Column('mq_status', String),
    #Column('mq_status', ENUM('normal','warning','urgent','accident')),
    Column('mq_created_at', DateTime(timezone=True), server_default=func.now()),
    schema=schema_name)




if __name__ == '__main__':

    if sys.argv[1] == 'migrate':
        try:
            metadata.create_all(engine)

            #classes = []
            #for i in inspect.getmembers(sys.modules[__name__], inspect.isclass):
            #    if '__main__' in str(i):
            #        classes.append(i[0])
            #class_list = ', '.join(classes)
            #print("model migration successfully completed! migrated models are :\n==> "+class_list)

        except:
            print("Something went wrong with model migration")


    if sys.argv[1] == 'initiate':
        try:
            print('a')
#            conn.execute(
#                mapping_column.insert(),[
#                    {'column_name_en':'seq','column_name_ko':'업체 고유번호', 'column_desc':'', 'column_sample_data':''},
#                    {'l_name':'yo','f_name':'alice'}
#                    ]
#                )
        except:
            None

    if sys.argv[1] == 'inspect':
#        try:
        inspector = inspect(engine)
            # Get table information
        tables = [table for table in inspector.get_table_names('columbus')]
        for table in tables:


            print(table)
            for i in inspector.get_columns(table, schema='columbus'):
                print("-----")
                print(i.keys())

            from itertools import chain
            from collections import defaultdict

            dict3 = defaultdict(list)
            print([column for column in inspector.get_column_names(table, schema='columbus')])
            for k,v in chain([column.items() for column in inspector.get_column_names(table, schema='columbus')]):
                dict3[k].append(v)

            for k,v in dict3.items():
                print(k,v)
            
 
    if sys.argv[1] == 'test':

        # CREATE Dummy table
        try:
            dummy = Table('Dummy', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('value', String, default='test_value'),
                    schema = schema_name
                    )

            dummy.create(engine)
        except: None
        inspector = inspect(engine)
        print(utils.string_align(_formatting_str='Dummy TABLE CREATE',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if 'Dummy' in inspector.get_table_names('columbus') else '[NO]'))




        dummy.drop(engine)
        inspector = inspect(engine)
        print(utils.string_align(_formatting_str='Dummy TABLE DROP',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if 'Dummy' not in inspector.get_table_names('columbus') else '[NO]'))
