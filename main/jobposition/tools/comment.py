#-*- coding:utf-8 -*-
# comment.py :: Handing text format


import sys, os

# Set execution base directory to be main 
sys.path.append(os.path.dirname(os.path.abspath(
                os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
                )))


HELP_MAIN = '''



     _ ^ _                     v                  |~         {date}
    '_\\V/ `             v                   |/    w
    ' oX`                    v             / (   (|   \\
       X                                  /( (/   |)  |\\
       X                           ____  ( (/    (|   | )   ,
       X                          |----\\ (/ |    /|   |'\\  /^;
       X.a##a.                    \\---*---Y--+-----+---+--/(
    .aa########a.>>                \\-------*---*--*---*--/
 .a################aa. ~~~~~~~~~~~~~~'~~~~~~~~~~~~~~~~~~'~~~~~~~~~~~~~~~~~~~~~~~~~~
  ______  __    __  .______       __       _______.___________.  ______   .______    __    __   _______ .______
 /      ||  |  |  | |   _  \     |  |     /       |           | /  __  \  |   _  \  |  |  |  | |   ____||   _  \\
|  ,----'|  |__|  | |  |_)  |    |  |    |   (----`---|  |----`|  |  |  | |  |_)  | |  |__|  | |  |__   |  |_)  |
|  |     |   __   | |      /     |  |     \   \       |  |     |  |  |  | |   ___/  |   __   | |   __|  |      /
|  `----.|  |  |  | |  |\  \----.|  | .----)   |      |  |     |  `--'  | |  |      |  |  |  | |  |____ |  |\  \----.
 \______||__|  |__| | _| `._____||__| |_______/       |__|      \______/  | _|      |__|  |__| |_______|| _| `._____|

The project named 'Christopher Columbus' was initially conducted in order to bring efficiency
into the business data collection process from various sources on the internet.

▶ Data process procedure on christopher:
    [ load ] --> [ Collect ] --> [ Render ]
    For more detail, execute "python3 christopher.py -c" on CLI

▶ COMMAND EXAMPLE
     _______________________________________________________________________________
    |                                                                               |
    | CHRISTOPHER GENERAL COMMAND EXAMPLES :                                        |
    |                                                                               |
    |   python3 christopher.py -h   |  python3 christopher.py --help                |
    |   python3 christopher.py -v   |  python3 christopher.py --version             |
    |   python3 christopher.py -t   |  python3 christopher.py --top_resource        |
    |   python3 christopher.py -s   |  python3 christopher.py --sqs_message         |
    |   python3 christopher.py -r   |  python3 christopher.py --renedered_s3        |
    |   python3 christopher.py -p   |  python3 christopher.py --purge_resource      |
    |                                                                               |
    | CHRISTOPHER EXECUTION COMMAND EXAMPLES (JOB-POSITION-TIME) :                  |
    |                                                                               |
    |   python3 christopher.py -j initializer -a singlerun -d now                   |
    |   python3 christopher.py -j [ producer | consumer ] -a collecting -d now      |
    |   python3 christopher.py -j [ producer | consumer ] -a rendering -d now       |
    |   python3 christopher.py --job producer --action rendering --actiondetail now |
    |_______________________________________________________________________________|

-----------
'''
JOB_ASSIGN = '''

                 |    |    |          [Step 1]
                )_)  )_)  )_)         항해를 위한 배가 준비되었습니다.
               )___))___))___)\\       배에 승선할 선원의 모집이 곧 진행될 예정입니다.
              )____)____)_____)\\
            _____|____|____|____\\\\__  모집 선원 포지션 : "{}"
        ----\\                   /-----------------------------------------------
        ^^^ ^^^^^^^^^^^^^^^^^^^^^        ^^^^         ^^             ^^^^
            ^^^^      ^^^^     ^^^    ^^  ^         ^^^^       ^^^
                 ^^^^      ^^^    ^^^^         ^^           ^^^^^^^^^^^
'''
