#-*- coding:utf-8 -*-
# utils.py :: Commonly used utilities are stored


# Return code 0 : Normal
# Return code 1 : Abnormal - Debugged







import time, random, string, sys, types, os
# Set execution base directory to be main
sys.path.append(os.path.dirname(os.path.abspath(
                os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
                )))

import requests
from datetime import datetime, timedelta
from pytz import timezone
from tabulate import tabulate
from inspect import getframeinfo, stack, getmembers
import pandas as pd
from urlextract import URLExtract
#def date_convert
import calendar
from jobposition.tools import config

def safe_int(_value):
    """
    safe_int(_value)
    
        안전한 숫자 자료형 반환
        : example => safe_int("241")
        : return => (0, 241)
    --------------------------------------------------------------------------------------------
    """
    try:
        safe_integer = _value if isinstance(_value, int) else int(_value)
        return (0, safe_integer)
    except:
        return (1, f'"{_value}" cannot be casted to integer')


def safe_float(_value):
    """
    safe_float(_value)
    
        안전한 부동소수점 자료형 반환
        : example => safe_float("2.14")
        : return => (0, 2.41)
    --------------------------------------------------------------------------------------------
    """
    try:
        safe_float = _value if isinstance(_value, float) else float(_value)
        return (0, safe_float)
    except:
        return (1, f'"{_value}" cannot be casted to float')


def debuginfo(_message):
    """
    debuginfo(_message)

        문제가 발생한 지점의 파일명, 라인번호, 에러메시지 확인
        : example   => debuginfo('Queue message production has failed')
        : return    => "filename:line linenumber - message"
        : arguments => _message = str
    --------------------------------------------------------------------------------------------
    """
    caller = getframeinfo(stack()[1][0])
    return "{}:line {} - {}".format(caller.filename, caller.lineno, _message)



def date_point(_tense='present', _tense_gap='0', _timezone='Asia/Seoul', _output='datetime_val'):

    """
    date_point(_tense='present', _tense_gap='0', _timezone='Asia/Seoul', _output='datetime_val')

        오늘을 기준으로 하는 과거 현재 미래 날짜 계산
        : example   => datepoint(_tense='present', _output='datetime_val')
        : return    => (1, '2019-09-16 19:06:02')
        : arguments => _tense = ['past', 'present', 'future']
                       _tense_gap = ['0', '1', '2', '3', ...]
                       _timezone = ['Asia/Seoul', 'Asia/Hong_Kong', 'America/New_York', ...]
                       _output = ['date_val', 'datetime_val', 'datetime']
    --------------------------------------------------------------------------------------------
    """

    if _tense == 'past':

        if int(_tense_gap) < 1:
            message = debuginfo('tense_gap must be greater than 0')
            return (1, message)

        else:
            raw_datetime = datetime.now(timezone(_timezone)) - timedelta(days=int(_tense_gap))


    elif _tense == 'present':

        if int(_tense_gap) == 0:
            raw_datetime = datetime.now(timezone(_timezone))

        else:
            message = debuginfo('present tense must have tense_gap of "0"')
            return (1, message)


    elif _tense == 'future':

        if int(_tense_gap) < 1:
            message = debuginfo('tense_gap must be greater than 0')
            return (1, message)

        else:
            raw_datetime = datetime.now(timezone(_timezone)) + timedelta(days=int(_tense_gap))


    else:
        message = debuginfo('tense for datetime is not selected')
        return (1, message)


    if _output == 'date_val':
        datetime_str = raw_datetime.strftime('%Y-%m-%d')

    elif _output == 'datetime_val':
        datetime_str = raw_datetime.strftime('%Y-%m-%d %H:%M:%S')

    elif _output == 'datetime':
        datetime_str = raw_datetime

    else:
        message = debuginfo('output format is not defined')
        return (1, message)

    return (0, datetime_str)





def string_align(_formatting_str ,_text_align='center', _expression='.', _gap='20'):
    """
    string_align(_formatting_str ,_text_align='center', _expression='.', _gap='20')

        정해진 문자열 사이즈 내에서 문자와 특수문자를 배열
        : example   => string_align(_formatting_str='intro', _text_align='center', _expression='=', _gap='30')
        : return    => (0, '============intro============')
        : arguments => _formatting_str = str
                       _text_align = ['left', 'right', 'center']
                       _expression = ['.', '_', '=', '*', ...]
                       _gap = int
    --------------------------------------------------------------------------------------------
    """

    if _text_align == 'left':
        location_sig = '<'

    elif _text_align == 'right':
        location_sig = '>'

    elif _text_align == 'center':
        location_sig = '^'

    else:
        message = debuginfo('function argument "text_align" is not properly defined')
        return (1, message)


    assembled = '{:'+ _expression + f'{location_sig}' + _gap + '}'

    str_insert = assembled.format(_formatting_str)

    return (0, str_insert)


def rangebatch_generator(start_seq, end_seq, batch_size):
    """
    rangebatch_generator(start_seq, end_seq, batch_size)
        
        수치 레인지 구간을 배치 사이즈로 나누어 반환해준다 (리스트의 리스트)
        : example   => rangebatch_generator(start_seq=1, end_seq=700000, batch_size=100000)
        : return    => (0, [range(1, 100001), range(100001, 200001), range(200001, 300001), ...])
        : arguments => start_seq = int
                       end_seq   = int
                       batch_size= int
    --------------------------------------------------------------------------------------------
    """

    def batch(iterable, n=1): # 배치 덤프 생성 // n 만큼 하나의 배치로
        l = len(iterable)
        for ndx in range(0, l, n):
            yield iterable[ndx:min(ndx + n, l)]

    start_seq = 1 if start_seq==None else start_seq

    try:
        start_seq   = start_seq if isinstance(start_seq, int)==True else int(start_seq)
        end_seq     = end_seq+1 if isinstance(end_seq, int)==True else int(end_seq)+1
        batch_size  = batch_size if isinstance(batch_size, int)==True else int(batch_size)

    except:
        return (1, 'casting error. all arguments have to have integer-like format')
            
    range_batch = [batch_dump for batch_dump in batch(range(start_seq, end_seq), batch_size)]
    return (0, range_batch)
 
 


def string_generator(_strlength):

    """
    string_generator(_strlength)
        
        주어진 길이에 맞게 무작위 문자열 생성 (대소문자 혼합)
        : example   => string_generator(5)
        : return    => 'kHiDk'
        : arguments => _strlength = int
    --------------------------------------------------------------------------------------------
    """

    try:
        letters = string.ascii_letters
    
        random_letter = ''.join(random.choice(letters) for i in range(_strlength))
    
        return (0, random_letter)

    except:
        message = debuginfo('random string is not generated')
        return (1, message)

def top_resource():
    None
def slack_message(_message):
    None


def table_pprint(_df, _tablefmt='psql', _limit='10'):

    """
    table_pprint(_df, _tablefmt='psql', _limit='10') 
        
        매트릭스나 데이터프레임을 보기좋게 출력 (https://pypi.org/project/tabulate/)
        : example   => print(table_pretty(_df=[["Name","Age"],["Alice",24],["Bob",19]], _tablefmt='psql'))
        : result    =>  +--------+-------+
                        | Name   |   Age |
                        |--------+-------|
                        | Alice  |    24 |
                        | Bob    |    19 |
                        +--------+-------+
        : arguments => _df = DataFrame
                       _tablefmt = ['plain', 'simple', 'github', 'grid', 'fancy_grid', 'pipe', 'orgtbl', 
                                    'jira', 'presto', 'psql', 'rst', 'mediawiki', 'moinmoin', 'youtrack',
                                    'html', 'latex', 'latex_raw', 'latex_booktabs', 'textile']
                       _limit = int
    --------------------------------------------------------------------------------------------    
    """

    if isinstance(_df, pd.DataFrame):

        table = tabulate(_df.head(_limit), headers='keys', tablefmt=_tablefmt)
        return (0, table)
        
    elif isinstance(_df, list):

        table = tabulate(_df, headers='firstrow', tablefmt=_tablefmt)
        return (0, table)

    else:
        message = debuginfo('DataFrame or List object has to be provided as input argument')
        return (1, message)


def url_status(_urlstring, _device):

    """
    url_status(_urlstring, _device)
     
        URL 헤더 요청 후 응답값 조회
        : example   => url_status(_urlstring='http://www.smartskin.co.kr', _device='mobile')
        : result    => (0, 200)
        : arguments => _urlstring = string
                       _device    = ['pc', 'mobile']
    --------------------------------------------------------------------------------------------    
    """
    if _device == 'pc':
        UA = config.PC_WINDOW_UA['chrome74']
    elif _device == 'mobile':
        UA = config.MOBILE_AND_UA['chrome74']
    else:
        print("device argument must be either 'pc' or 'mobile'.")
        return None

    urlstring   = _urlstring if _urlstring.startswith('http') else ('http://' + _urlstring)
    headers     = {'user-agent': UA}
    try:
        response    = requests.head(urlstring, headers=headers)
        status      = response.status_code
    except:
        return (1, None)

    if (str(status).startswith('2') or str(status).startswith('3')):
        return (0, status)
    else:
        return (1, status)




def url_extractor(_urlstring):
    """
     url_extractor(_urlstring)

        문자열에서 URL 주소를 추출하여 리스트로 반환
        : example   => url_extractor('@$#@$%, www.abc.com, fsdk')
        : result    => (0, ['www.abc.com'])
        : arguments => _urlstring = string
    --------------------------------------------------------------------------------------------    
    """
    try:
        extractor = URLExtract()
        valid_url = extractor.find_urls(_urlstring)
        return (0, valid_url)
    except:
        return (1, 'fail')






def datetime_validator(_datetime_val):
    """
     datetime_validator(_datetime_val)

        8자리 숫자형 날짜 데이터의 범위 재정비 (DB 입력시 Range 오류 방지)
        : example   => datetime_validator('20993299')
        : result    => (0, [2019, 12, 31])
        : arguments => _datetime_val = int
    --------------------------------------------------------------------------------------------    
    """    
    
    try:
        _datetime_val = str(_datetime_val) if isinstance(_datetime_val, int) else _datetime_val
    except:
        return (1, 'variable casting error')
    
    months = [i+1 for i in range(12)]
    if len(_datetime_val) == 8:
        year  = int(_datetime_val[:4])
        month = int(_datetime_val[4:6])
        day   = int(_datetime_val[6:])
        
        thisyear    = datetime.today().year
        year        = thisyear if year > thisyear else year
        month       = month if month in months else 12
        month_str   = '0' + str(month) if month < 10 else str(month)

        max_day = calendar.monthrange(year, month)[1]
        day     = max_day if day > max_day else day
        day_str = '0' + str(day) if day < 10 else str(day)

        return (0, [str(year), month_str, day_str])

    else:
        return (1, 'input argument must have length of 8')






if __name__ == "__main__":

    if sys.argv[1] == 'test':

        print(string_align(
                _formatting_str='Function "datepoint" past',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if date_point(_tense='past',_tense_gap='3')[0]==0 else '[{}]'.format(
                        date_point(_tense='past',_tense_gap='3')[1])
                    )
                )

        print(string_align(
                _formatting_str='Function "datepoint" present',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if date_point(_tense='present')[0]==0 else '[{}]'.format(
                        date_point(_tense='present')[1])
                    )
                )

        print(string_align(
                _formatting_str='Function "datepoint" future',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if date_point(_tense='future',_tense_gap='3')[0]==0 else '[{}]'.format(
                        date_point(_tense='future',_tense_gap='3')[1])
                    )
                )

        print(string_align(
                _formatting_str='Function "string_generator"',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if string_generator(10)[0]==0 else '[{}]'.format(
                        string_generator(10)[1])
                    )
                )

        print(string_align(
                _formatting_str='Function "table_pprint"',_text_align='left',_expression='.',_gap='50')[1]
                + ('[OK]' if table_pprint(_df=[["Name","Age"],["Sean",24]], _tablefmt='psql')[0]==0 else '[{}]'.format(
                        table_pprint(_df=[["Name","Age"],["Sean",24]], _tablefmt='psql')[1])
                    )
                )

    if sys.argv[1] == 'functions':

        current_filename = stack()[0][1]
        current_linesize = stack()[0][2]
        print(string_align(_formatting_str=current_filename, _text_align='center', _expression='=', _gap='100')[1])

        def is_local(object):
            
            """
    로컬함수 모듈을 스캔 - 직접 호출해서 사용되지 않는다.
    --------------------------------------------------------------------------------------------
            """
            return isinstance(object, types.FunctionType) and object.__module__ == __name__

        local_func = [name for name, value in getmembers(sys.modules[__name__], predicate=is_local)]
        for enum, func in enumerate(local_func):
            print(str(enum+1)+'. '+func)
            print(eval(func).__doc__)

