#-*- coding:utf-8 -*-
# awsutils.py :: Handling AWS APIs through SDK provided by AWS


import sys, os

# Set execution base directory to be main 
sys.path.append(os.path.dirname(os.path.abspath(
				os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
				)))

import boto3
from jobposition.tools import config, utils
from datetime import datetime

class AWSBoto3:
	"""
	DESC : 
	"""
	def __init__(self):

		self.__ACCESS_KEY   = config.ACCESS_KEY
		self.__SECRET_KEY   = config.SECRET_KEY
		self.__REGION_NAME  = config.REGION_NAME




	#SQS methods

	def sqs(self, _queue_type='collect'):
		"""
		_queue_type : collect, render
		"""

		sqs_object = boto3.client(
				'sqs',
				region_name             = self.__REGION_NAME,
				aws_access_key_id       = self.__ACCESS_KEY,
				aws_secret_access_key   = self.__SECRET_KEY
				)


		if _queue_type == 'collect':
			queue_url = config.SQS_QUEUE_URL['COLLECT']
			return (0, sqs_object, queue_url)


		elif _queue_type == 'render':
			queue_url = config.SQS_QUEUE_URL['RENDER']
			return (0, sqs_object, queue_url)


		else:
			message = utils.debuginfo('Queue type must be defined')
			return (1, message)




	def sqs_active_queue(self):
		
		sqs_clinet = sqs()		

		sqs_object = sqs_client[1] if sqs_clinet[0] == 0 else None

		if sqs_object == None:
			return (1, utils.debuginfo('fail to create sqs client object'))
		
		active_queues = sqs_object.list_queues(QueueNamePrefix='standard')


		try:
			sqs_active_list = list()
			for enum, url in enumerate(active_queues['QueueUrls']):
				enum = enum + 1
				
				delimiter = 'standard'			
				queue_name = delimiter + url.split(delimiter)[1]
				response = sqs_object.get_queue_attributes(QueueUrl=url, AttributeNames=['All',])

				created = (datetime.fromtimestamp(
								int(response['Attributes']['CreatedTimestamp'])).strftime('%Y-%m-%d %H:%M:%S'))
				modified = (datetime.fromtimestamp(
								int(response['Attributes']['LastModifiedTimestamp'])).strftime('%Y-%m-%d %H:%M:%S'))
				queued_msg = response['Attributes']['ApproximateNumberOfMessages']
				queuearn = response['Attributes']['QueueArn']

				queue_detail = ("\n{enum_}] ({name_}) => '{url_}'\n".format(enum_=enum, name_=queue_name, url_=url)
									+ "  ► Queue info: {}\n".format(queuearn)
									+ "  ► Number of messages on queue: {}\n".format(queued_msg)
									+ "  ► Created at: {}\n".format(created)
									+ "  ► Modified at: {}\n".format(modified)
								)
				sqs_active_list.append(queue_detail)
			return (0, sqs_active_list)
		except:
			return (1, utils.debuginfo('something went wrong with retrieving sqs active queues'))
			
			
			
			
	def sqs_queue_purge(self, _target, _action='confirm'):
		"""
		_target  : collect, render
		_action  : confirm, proceed
		"""
		
		if (_target=='collect') and (_action=='confirm'):
			try:
				# confirmation 을 위해 대상 sqs url 반환 ==> 갯수가 나오면 베스트!
				sqs_client = sqs(_queue_type='collect')
				sqs_url = sqs_client[2] if sqs_client[0]==0 else None
				if sqs_url == None:
					return (1, utils.debuginfo('fail to create sqs client object'))
					
				return (0, sqs_url)
			except:
				return (1, utils.debuginfo('something went wrong with confirmation'))
				
				
		elif (_target=='collect') and (_action=='proceed'):
			try:
				sqs_client = sqs(_queue_type='collect')
				rescode,sqs_object,sqs_url = sqs_client if sqs_client[0]==0 else (None,None,None)

				if sqs_object == None:
					return (1, utils.debuginfo('fail to create sqs client object'))
				
				sqs_object.purge_queue(QueueUrl=sqs_url)
				return (0, 'All messages from the queue are removed completely')
			except:
				return (1, utils.debuginfo('something went wrong with purging queue'))


		elif (_target=='render') and (_action=='confirm'):
			try:
				# confirmation 을 위해 대상 sqs url 반환
				sqs_client = sqs(_queue_type='render')
				sqs_url = sqs_client[2] if sqs_client[0]==0 else None
				if sqs_url == None:
					return (1, utils.debuginfo('fail to create sqs client object'))
				
				return (0, sqs_url)
			except:
				return (1, utils.debuginfo('something went wrong with confirmation'))
				
				
		elif (_target=='render') and (_action=='proceed'):
			try:
				sqs_client = sqs(_queue_type='render')
				rescode,sqs_object,sqs_url = sqs_client if sqs_client[0]==0 else (None,None,None)
				if sqs_object==None:
					return (1, utils.debuginfo('fail to create sqs client object'))

				sqs_object.purge_queue(QueueUrl=sqs_url)
				return (0, 'All messages from the queue are removed completely')
			except:
				return (1, utils.debuginfo('something went wrong with purging queue'))
		
		else:
			return(1, utils.debuginfo('Message Purge process is unexpectedly terminated'))





	# S3 methods


	def s3_client(self):

		try:
			s3_client_object = boto3.client(
					's3',
					aws_access_key_id       = self.__ACCESS_KEY,
					aws_secret_access_key   = self.__SECRET_KEY
					)
			return (0, s3_client_object)

		except:
			message = utils.debuginfo('s3 client object is not generated')
			return (1, message)


	def s3_resource(self):

		try:
			s3_resource_object = boto3.resource(
					's3',
					aws_access_key_id       = self.__ACCESS_KEY,
					aws_secret_access_key   = self.__SECRET_KEY
					)
			return (0, s3_resource_object)

		except:
			message = utils.debuginfo('s3 resource object is not generated')
			return (1, message)





	def s3_active_bucket(self):
		return None
#		
#		s3 = s3_resource()
#		s3_object = s3[1] if s3[0]==0 else return (1, utils.debuginfo('s3 client object is not generated'))
#		None
#
#	def s3_bucket_purge():
#		None
#
#	def s3_bucket_summary():
#		None




if __name__ == "__main__":

	if sys.argv[1] == 'test':

		print(utils.string_align(_formatting_str='Boto3 RESOURCE',_text_align='center',_expression='-',_gap='50')[1])


		boto_class = AWSBoto3()

		print(utils.string_align(
				_formatting_str='SQS SOURCE OBJECT',
				_text_align='left',
				_expression='.',
				_gap='50')[1]

				+ ('[OK]' if boto_class.sqs(_queue_type='collect')[0]==0 else '[{}]'.format(
					boto_class.sqs(_queue_type='collect')[1])
					)
				)

		print(utils.string_align(
				_formatting_str='SQS COLLECTION OBJECT',
				_text_align='left',
				_expression='.',
				_gap='50')[1]

				+ ('[OK]' if boto_class.sqs(_queue_type='render')[0]==0 else '[{}]'.format(
					boto_class.sqs(_queue_type='render')[1])
					)
				)

		print(utils.string_align(
				_formatting_str='S3 CLIENT OBJECT',
				_text_align='left',
				_expression='.',
				_gap='50')[1]

				+ ('[OK]' if boto_class.s3_client()[0]==0 else '[{}]'.format(
					boto_class.sqs(_queue_type='render')[1])
					)
				)

		print(utils.string_align(
				_formatting_str='S3 RESOURCE OBJECT',
				_text_align='left',
				_expression='.',
				_gap='50')[1]

				+ ('[OK]' if boto_class.s3_resource()[0]==0 else '[{}]'.format(
					boto_class.sqs(_queue_type='render')[1])
					)
				)
