import sys, os

# Set execution base directory to be main 
sys.path.append(os.path.dirname(os.path.abspath(
                os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
                )))


from jobposition.tools.models import session, MappingColumn, MappingMgmtStatus, MappingSellMethod, MappingCategory



def mapping_initiate():
    try:

        mapping_rec1 = MappingColumn(col_name_en='resultCode',col_name_ko='결과코드',
                               col_desc='요청에 따른 응답 결과 코드 정보',col_sample_data = '"00"')
        mapping_rec2 = MappingColumn(col_name_en='resultMag',col_name_ko='결과메시지',
                               col_desc='결과 코드에 해당하는 메시지 정보',col_sample_data = '"NORMAL SERVICE"')
        mapping_rec3 = MappingColumn(col_name_en='seq',col_name_ko='업체고유번호',
                               col_desc='각 업체에 할당되는 고유한 시퀀스 번호',col_sample_data = '"3241"')
        mapping_rec4 = MappingColumn(col_name_en='bup_nm',col_name_ko='상호명',
                               col_desc='상호명 정보',col_sample_data = '"유니드컴즈"')
        mapping_rec5 = MappingColumn(col_name_en='addr',col_name_ko='사업장소재지',
                               col_desc='사업장 소재지 정보',col_sample_data = '"서울특별시 마포구 양화로 61"')
        mapping_rec6 = MappingColumn(col_name_en='newaddr',col_name_ko='사업장소재지(도로명)',
                               col_desc='도로명 사업장 소재지 정보',col_sample_data = '"서울특별시 금천구 가산디지털1로 212, 306호 (가산동, 코오롱디지털타워애스턴)"')
        mapping_rec7 = MappingColumn(col_name_en='perm_ymd',col_name_ko='신고일자',
                               col_desc='통신판매업 신고일자',col_sample_data = '"20190101"')
        mapping_rec8 = MappingColumn(col_name_en='bup_reg_no',col_name_ko='법인사업자등록번호',
                               col_desc='법인등록번호 정보, 앞에 7자리까지 제공',col_sample_data = '"1801110******"')
        mapping_rec9 = MappingColumn(col_name_en='wrkr_no',col_name_ko='사업자등록번호',
                               col_desc='사업자등록번호 정보',col_sample_data = '"4108654518"')
        mapping_rec10 = MappingColumn(col_name_en='org_and_team_nm',col_name_ko='신고기관명',
                               col_desc='통신판매업 신고 기관명 정보',col_sample_data = '"서울특별시 금천구"')
        mapping_rec11 = MappingColumn(col_name_en='repsnt_nm',col_name_ko='사업자명',
                               col_desc='사업자 이름 정보',col_sample_data = '"홍길동"')
        mapping_rec12 = MappingColumn(col_name_en='rep_telno',col_name_ko='신고기관대표연락처',
                               col_desc='신고기관 대표연락처 정보',col_sample_data = '"042-611-6703"')
        mapping_rec13 = MappingColumn(col_name_en='tre_artcl',col_name_ko='취급품목',
                               col_desc='취급품목에 대한 상세 항목 확인',col_sample_data = '"06"')
        mapping_rec14 = MappingColumn(col_name_en='sil_met',col_name_ko='판매방식',
                               col_desc='판매방식에 대한 상세 항목 확인',col_sample_data = '"02"')
        mapping_rec15 = MappingColumn(col_name_en='dmn_nm',col_name_ko='인터넷도메인',
                               col_desc='인터넷 도메인 정보 (직접 입력)',col_sample_data = '"www.uneedcomms.com"')
        mapping_rec16 = MappingColumn(col_name_en='mngStateCode',col_name_ko='영업상태코드',
                               col_desc='영업상태코드에 대한 상세 항목 확인',col_sample_data = '"01"')
        mapping_rec17 = MappingColumn(col_name_en='bup_yn',col_name_ko='법인여부',
                               col_desc='법인사업장 여부 확인 정보 (0 : 개인, 1 : 법인)',col_sample_data = '"0"')
        mapping_rec18 = MappingColumn(col_name_en='apv_perm_mgt_no',col_name_ko='통신판매번호',
                               col_desc='통신판매번호 정보',col_sample_data = '"2010-부산남구-0000"')
        mapping_rec19 = MappingColumn(col_name_en='host_srv_addr',col_name_ko='호스트서버소재지',
                               col_desc='서버호스팅 지점명',col_sample_data = '"서울특별시 관악구 신림동 527번지 13호"')
        mapping_rec20 = MappingColumn(col_name_en='numOfRows',col_name_ko='페이지당항목수',
                               col_desc='페이지당 items > item 객체의 항목 수',col_sample_data = '"1"')
        mapping_rec21 = MappingColumn(col_name_en='pageNo',col_name_ko='페이지',
                               col_desc='페이지 수 정보',col_sample_data = '"1"')
        mapping_rec22 = MappingColumn(col_name_en='totalCount',col_name_ko='모든항목수',
                               col_desc='전체 항목 수 정보',col_sample_data = '"1"')
        mapping_rec23 = MappingColumn(col_name_en='sidoNm',col_name_ko='지역',
                               col_desc='지역 정보',col_sample_data = '"서울특별시"')

        mapping_mgmt_rec1 = MappingMgmtStatus(mgmt_status_code='01', mgmt_status_desc='통신판매업 신고')
        mapping_mgmt_rec2 = MappingMgmtStatus(mgmt_status_code='02', mgmt_status_desc='통신판매업 휴업')
        mapping_mgmt_rec3 = MappingMgmtStatus(mgmt_status_code='03', mgmt_status_desc='통신판매업 폐업')
        mapping_mgmt_rec4 = MappingMgmtStatus(mgmt_status_code='04', mgmt_status_desc='직권취소')
        mapping_mgmt_rec5 = MappingMgmtStatus(mgmt_status_code='05', mgmt_status_desc='타시군구이관')
        mapping_mgmt_rec6 = MappingMgmtStatus(mgmt_status_code='06', mgmt_status_desc='타시군구전입')
        mapping_mgmt_rec7 = MappingMgmtStatus(mgmt_status_code='07', mgmt_status_desc='직권말소')
        mapping_mgmt_rec8 = MappingMgmtStatus(mgmt_status_code='08', mgmt_status_desc='영업재개')

        mapping_sell_rec1 = MappingSellMethod(method_code='01', method_desc='TV홈쇼핑')
        mapping_sell_rec2 = MappingSellMethod(method_code='02', method_desc='인터넷')
        mapping_sell_rec3 = MappingSellMethod(method_code='03', method_desc='카탈로그')
        mapping_sell_rec4 = MappingSellMethod(method_code='04', method_desc='신문잡지')
        mapping_sell_rec5 = MappingSellMethod(method_code='05', method_desc='기타')

        mapping_cate1 = MappingCategory(cate_code='01', cate_desc='종합몰')
        mapping_cate2 = MappingCategory(cate_code='02', cate_desc='교육 / 도서 / 완구 / 오락')
        mapping_cate3 = MappingCategory(cate_code='03', cate_desc='가전')
        mapping_cate4 = MappingCategory(cate_code='04', cate_desc='컴퓨터 / 사무용품')
        mapping_cate5 = MappingCategory(cate_code='05', cate_desc='가구 / 수납용품')
        mapping_cate6 = MappingCategory(cate_code='06', cate_desc='의류 / 패션 / 잡화 / 뷰티')
        mapping_cate7 = MappingCategory(cate_code='07', cate_desc='레저 / 여행 / 공연')
        mapping_cate8 = MappingCategory(cate_code='08', cate_desc='건강 / 식품')
        mapping_cate9 = MappingCategory(cate_code='09', cate_desc='성인 / 성인용품')
        mapping_cate10 = MappingCategory(cate_code='10', cate_desc='자동차 / 자동차용품')
        mapping_cate11 = MappingCategory(cate_code='11', cate_desc='상품권')
        mapping_cate12 = MappingCategory(cate_code='12', cate_desc='기타')

        session.bulk_save_objects([
                mapping_rec1, mapping_rec2, mapping_rec3, mapping_rec4, mapping_rec5, mapping_rec6,
                mapping_rec7, mapping_rec8, mapping_rec9, mapping_rec10, mapping_rec11, mapping_rec12,
                mapping_rec13, mapping_rec14, mapping_rec15, mapping_rec16, mapping_rec17, mapping_rec18,
                mapping_rec19, mapping_rec20, mapping_rec21, mapping_rec22,  mapping_rec23,

                mapping_mgmt_rec1, mapping_mgmt_rec2, mapping_mgmt_rec3, mapping_mgmt_rec4,
                mapping_mgmt_rec5, mapping_mgmt_rec6, mapping_mgmt_rec7, mapping_mgmt_rec8,

                mapping_sell_rec1, mapping_sell_rec2, mapping_sell_rec3, mapping_sell_rec4,

                mapping_cate1, mapping_cate2, mapping_cate3, mapping_cate4, mapping_cate5, mapping_cate6,
                mapping_cate7, mapping_cate8, mapping_cate9, mapping_cate10, mapping_cate11, mapping_cate12
                ])
        session.commit()
        return "success"
    except:
        print("error occurred")
        return None
