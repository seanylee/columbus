#-*- coding:utf-8 -*-
# models.py :: DATABASE MODEL MAPPING

import sys, os

# Set execution base directory to be main 
sys.path.append(os.path.dirname(os.path.abspath(
                os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
                )))


from jobposition.tools import config, initiate

import sqlalchemy

from sqlalchemy.ext.declarative import declarative_base # Database mapping declare

from sqlalchemy import Column, Integer, String, Boolean, DateTime, update # Column elements, to  be mapped to DB model
from sqlalchemy import create_engine
from sqlalchemy import ForeignKey # Relationship define
from sqlalchemy import create_engine # engine creation
from sqlalchemy import MetaData, Table

from sqlalchemy.orm import sessionmaker,scoped_session # Session manamgement
from sqlalchemy.orm import relationship
from sqlalchemy.orm import exc, column_property

from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.dialects.postgresql import ARRAY

from sqlalchemy.sql import func


engine = create_engine(
                        'postgresql://{user}:{password}@{host}:5432/{db}'.format(
                                user        =config.POSTGRESQL['user'],
                                password    =config.POSTGRESQL['pass'],
                                host        =config.POSTGRESQL['host'],
                                db          =config.POSTGRESQL['name']
                            ),
                            client_encoding='utf-8'
                            , echo=True
                        )

session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

metadata = MetaData()
Base = declarative_base()


class AcquiredPublicSource(Base):
    __tablename__   = 'acquired_public_source'
    __table_args__  = {'schema' : 'columbus'}

    pubs_id                     = Column(Integer, primary_key=True, autoincrement=True)
    pubs_aggregate_key          = Column('pubs_aggregate_key', String, unique=True)

    pubs_sqs_delivered          = Column('pubs_sqs_delivered', Boolean, nullable=False, default=False)
    pubs_provide_organiz        = Column('pubs_provide_organiz', String)
    pubs_type                   = Column('pubs_type', String)
    pubs_type_detail            = Column('pubs_type_detail', String)
    pubs_type_extra             = Column('pubs_type_extra', String)
    pubs_seq                    = Column('pubs_seq', Integer)
    pubs_gen_type               = Column('pubs_gen_type', String)
    pubs_created_at             = Column('pubs_created_at', 
                                            DateTime(timezone=True),
                                            nullable=False,
                                            server_default=func.now()
                                        )


class AcquiredPrivateSource(Base):
    __tablename__   = 'acquired_private_source'
    __table_args__  = {'schema' : 'columbus'}

    pris_id                     = Column(Integer, primary_key=True, autoincrement=True)
    pris_aggregate_key          = Column('pris_aggregate_key', String, unique=True)

    pris_collection_delivered   = Column('pris_collection_delivered', Boolean, default=False)
    pris_provide_organiz        = Column('pris_provide_organiz', String)
    pris_type                   = Column('pris_type', String)
    pris_type_detail            = Column('pris_type_detail', String)
    pris_business_name          = Column('pris_business_name', String)
    pris_business_num           = Column('pris_business_num', Integer)
    pris_domain                 = Column('pris_domain', String)
    pris_created_at             = Column('pris_created_at', 
                                            DateTime(timezone=True),
                                            nullable=False,
                                            server_default=func.now()
                                        )


class AcquiredYourselfSource(Base):
    __tablename__   = 'acquired_yourself_source'
    __table_args__  = {'schema' : 'columbus'}

    yous_id                     = Column(Integer, primary_key=True, autoincrement=True)
    yous_aggregate_key          = Column('yous_aggregate_key', String, unique=True)

    yous_collection_delivered   = Column('yous_collection_delivered', Boolean, default=False)
    yous_provide_organiz        = Column('yous_provide_organiz', String)
    yous_type                   = Column('yous_type', String)
    yous_type_detail            = Column('yous_type_detail', String)
    yous_business_name          = Column('yous_business_name', String)
    yous_business_num           = Column('yous_business_num', Integer)
    yous_domain                 = Column('yous_domain', String)
    yous_created_at             = Column('yous_created_at', 
                                            DateTime(timezone=True),
                                            nullable=False,
                                            server_default=func.now()
                                        )


class BusinessDataCollection(Base):
    __tablename__   = 'business_data_collection'
    __table_args__  = {'schema' : 'columbus'}

    bdc_id                      = Column(Integer, primary_key=True, autoincrement=True)

    pris                        = relationship("AcquiredPrivateSource", uselist=False)
    pris_id                     = Column(Integer, ForeignKey('columbus.acquired_private_source.pris_id'), unique=True)

    yous                        = relationship("AcquiredYourselfSource", uselist=False)
    yous_id                     = Column(Integer, ForeignKey('columbus.acquired_yourself_source.yous_id'), unique=True)

    sqs_consumer                = relationship("SqsConsumer", uselist=False)
    sqs_consumer_id             = Column(Integer, ForeignKey('columbus.sqs_consumer.sqs_consumer_id'))

    bdc_source_type             = Column('bdc_source_type', String)
    bdc_business_name           = Column('bdc_business_name', String)
    bdc_business_no             = Column('bdc_business_no', Integer)
    bdc_business_repsnt         = Column('bdc_business_repsnt', String)
    bdc_mgmt_state              = Column('bdc_mgmt_state', String)
    bdc_business_addr           = Column('bdc_business_addr', String)
    bdc_business_newaddr        = Column('bdc_business_newaddr', String)
    bdc_business_tel            = Column('bdc_business_tel', String)
    bdc_url_all                 = Column('bdc_url_all', ARRAY(String))
    bdc_url_loaded              = Column('bdc_url_loaded', Boolean, default=False)
    bdc_established_date        = Column('bdc_established_date', String)
    bdc_raw_message             = Column('bdc_raw_message', JSON)
    bdc_created_at              = Column('bdc_created_at',
                                            DateTime(timezone=True),
                                            nullable=False,
                                            server_default=func.now()
                                        )


class BusinessDataCollectionUrl(Base):
    __tablename__   = 'business_data_collection_url'
    __table_args__  = {'schema' : 'columbus'}

    burl_id                 = Column(Integer, primary_key=True, autoincrement=True)

    bdc                     = relationship('BusinessDataCollection', uselist=False)
    bdc_id                  = Column(Integer, ForeignKey('columbus.business_data_collection.bdc_id'))

    burl_last_rendered_at   = Column('burl_last_rendered_at', DateTime(timezone=True), onupdate=func.now())
    burl_pc_status          = Column('burl_pc_status', Integer)
    burl_pc_render_cnt      = Column('burl_pc_render_cnt', Integer, default=0)
    burl_mobile_status      = Column('burl_mobile_status', Integer)
    burl_mobile_render_cnt  = Column('burl_mobile_render_cnt', Integer, default=0)
    burl_raw                = Column('burl_raw', String)
    burl_scheme             = Column('burl_scheme', String)
    burl_domain             = Column('burl_domain', String)
    burl_path               = Column('burl_path', String)
    burl_parameter          = Column('burl_parameter', String)
    burl_query              = Column('burl_query', String)
    burl_created_at         = Column('burl_created_at',
                                        DateTime(timezone=True),
                                        nullable=False,
                                        server_default=func.now()
                                    )



class BusinessDataRendering(Base):
    __tablename__   = 'business_data_rendering'
    __table_args__  = {'schema' : 'columbus'}

    bdr_id                      = Column(Integer, primary_key=True, autoincrement=True)

    sqs_consumer                = relationship("SqsConsumer", uselist=False)
    sqs_consumer_id             = Column(Integer, ForeignKey('columbus.sqs_consumer.sqs_consumer_id'), unique=True)

    bdr_ua_type                 = Column('bdr_ua_type', String)
    bdr_domain                  = Column('bdr_domain', String)
    bdr_origin_html_length      = Column('bdr_origin_html_length', Integer)
    bdr_status_code             = Column('bdr_status_code', Integer)
    bdr_header_raw              = Column('bdr_header_raw', String)
    bdr_header_response_time    = Column('bdr_header_response_time',Integer)
    bdr_body_response_time      = Column('bdr_body_response_time',Integer)
    bdr_rendering_time          = Column('bdr_rendering_time',Integer)
    bdr_body_uploaded           = Column('bdr_body_uploaded',Boolean, default=False)
    bdr_body_key                = Column('bdr_body_key', String)
    bdr_created_at              = Column('bdr_created_at',
                                            DateTime(timezone=True),
                                            nullable=False,
                                            server_default=func.now()
                                        )

class SqsConsumer(Base):
    __tablename__   = 'sqs_consumer'
    __table_args__  = {'schema' : 'columbus'}

    sqs_consumer_id             = Column(Integer, primary_key=True, autoincrement=True)
    
    sqs_producer                = relationship("SqsProducer", uselist=False)
    sqs_producer_id             = Column(Integer, ForeignKey('columbus.sqs_producer.sqs_producer_id'))
    
    sqs_seq                     = Column('sqs_seq', Integer)
    sqs_job_from                = Column('sqs_job_from', String)
    sqs_consumed                = Column('sqs_consumed', Boolean, default=False)
    sqs_consumed_pid            = Column('sqs_consumed_pid',Integer)
    mq_termin_code              = Column('mq_termin_code', String)
    sqs_consumed_at             = Column('sqs_consumed_at', DateTime(timezone=True), server_default=func.now())


class SqsProducer(Base):
    __tablename__   = 'sqs_producer'
    __table_args__  = {'schema' : 'columbus'}

    sqs_producer_id         = Column(Integer, primary_key=True, autoincrement=True)

    pubs                    = relationship("AcquiredPublicSource", uselist=False)
    pubs_id                 = Column(Integer, ForeignKey('columbus.acquired_public_source.pubs_id'), unique=True)
    burl                    = relationship("BusinessDataCollectionUrl", uselist=False)
    burl_id                 = Column(Integer, ForeignKey('columbus.business_data_collection_url.burl_id'))

    sqs_seq                 = Column('sqs_seq', Integer)
    sqs_job_from            = Column('sqs_job_from', String)
    sqs_produced            = Column('sqs_produced', Boolean, default=False)
    sqs_produced_pid        = Column('sqs_produced_pid',Integer)
    mq_termin_code          = Column('mq_termin_code', String)
    sqs_produced_at         = Column('sqs_produced_at', DateTime(timezone=True), server_default=func.now())

class SqsTerminationRef(Base):
    __tablename__   = 'sqs_termination_ref'
    __table_args__  = {'schema' : 'columbus'}

    mq_termin_id                = Column(Integer, primary_key=True, autoincrement=True)
    mq_termin_code              = Column('mq_termin_code', String)
    mq_termin_num               = Column('mq_termin_num', Integer)
    mq_termin_letter            = Column('mq_termin_letter', String)
    mq_termin_letter_detail     = Column('mq_termin_letter_detail', String)
    mq_termin_detail            = Column('mq_termin_detail', String)
    mq_status                   = Column('mq_status', String)
    test_mq                     = Column('mq_test', String)
    mq_created_at               = Column('mq_created_at', DateTime(timezone=True), server_default=func.now())



class MappingColumn(Base):
    __tablename__   = 'mapping_column'
    __table_args__  = {'schema' : 'columbus'}

    col_id                      = Column(Integer, primary_key=True, autoincrement=True)
    col_name_en                 = Column('col_name_en', String, unique=True)
    col_name_ko                 = Column('col_name_ko', String)
    col_desc                    = Column('col_desc', String)
    col_sample_data             = Column('col_sample_data', String)


class MappingMgmtStatus(Base):
    __tablename__   = 'mapping_mgmt_status'
    __table_args__  = {'schema' : 'columbus'}

    mgmt_id                     = Column(Integer, primary_key=True, autoincrement=True)
    mgmt_status_code            = Column('mgmt_status_code', String, unique=True)
    mgmt_status_desc            = Column('mgmt_status_desc', String)

class MappingSellMethod(Base):
    __tablename__   = 'mapping_sell_method'
    __table_args__  = {'schema' : 'columbus'}

    method_id                   = Column(Integer, primary_key=True, autoincrement=True)
    method_code                 = Column('method_code', String, unique=True)
    method_desc                 = Column('method_desc', String)


class MappingCategory(Base):
    __tablename__   = 'mapping_category'
    __table_args__  = {'schema' : 'columbus'}

    cate_id                     = Column(Integer, primary_key=True, autoincrement=True)
    cate_code                   = Column('cate_code', String, unique=True)
    cate_desc                   = Column('cate_desc', String)




if __name__ == '__main__':

    if sys.argv[1] == 'migrate':
        try:
            Base.metadata.create_all(bind=engine)
            print("Model migration has completed!")
        except:
            print("Something went wrong with model migration")


    elif sys.argv[1] == 'initiate':
        initiate.mapping_initiate()
        print("mapping initiate has completed")

    elif sys.argv[1] == 'test':
        print(repr(metadata.tables['columbus']))

