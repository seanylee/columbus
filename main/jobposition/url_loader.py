import sys
import os
import ast
import requests
import json
import uuid
from urllib.parse import urlparse

sys.path.append(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
        )


from jobposition.tools import config, awsutils, utils
from jobposition.tools.models import *



def business_data_url_loader():
    url_collection = [(inst.bdc_id, inst.bdc_url_all) for inst in session.query(BusinessDataCollection).filter(
                        BusinessDataCollection.bdc_url_all != None)]


    for row in session.query(BusinessDataCollection).filter(BusinessDataCollection.bdc_url_all != None):
        _bdc_id      = row.bdc_id
        _url_all     = row.bdc_url_all


        for each_url in _url_all:
            if each_url.startswith('http')==False:
                each_url = 'http://' + each_url

            purl        = urlparse(each_url)
            scheme      = purl.scheme
            domain      = purl.netloc
            path        = purl.path
            parameter   = purl.params
            query       = purl.query


            target_url      = scheme + '://' + domain
            pc_status       = utils.url_status(_urlstring=target_url, _device='pc')[1]
            mobile_status   = utils.url_status(_urlstring=target_url, _device='mobile')[1]


            session.add(
                BusinessDataCollectionUrl(
                    bdc_id              = _bdc_id,
                    burl_sqs_delivered  = False,
                    burl_raw            = each_url,
                    burl_scheme         = scheme,
                    burl_domain         = domain,
                    burl_path           = path,
                    burl_parameter      = parameter,
                    burl_query          = query,
                    burl_pc_status      = pc_status,
                    burl_mobile_status  = mobile_status
                    )
                )


    try:
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()



if __name__ == "__main__":
    business_data_url_loader()
