import sys, os, ast
import json
import uuid
sys.path.append(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
        )
from jobposition.tools import config, awsutils, utils
from jobposition.tools.models import *


def sqs_batch_message(sqs_object, sqs_url):
    print("sqs_batch_message started")
    batch_max = config.SQS_BATCH_MAX # config의 변수값 호출 BOTO3 문서에 따르면 배치 메시지는 10개가 최대!

    production_target = list()
    for inst in session.query(AcquiredPublicSource).filter_by(pubs_sqs_delivered=False):
        pubs_id                 = inst.pubs_id if inst.pubs_id else ''
        pubs_provide_organiz    = inst.pubs_provide_organiz if inst.pubs_provide_organiz else ''
        pubs_type               = inst.pubs_type if inst.pubs_type else ''
        pubs_type_detail        = inst.pubs_type_detail if inst.pubs_type_detail else ''
        pubs_seq                = inst.pubs_seq if inst.pubs_seq else ''
        url_without_seq         = '/'.join([pubs_provide_organiz, pubs_type, pubs_type_detail])
        production_target.append((pubs_id, pubs_seq, url_without_seq))


    listed_batch_list = list()
    for batch_step in range(0,len(production_target), batch_max):
        batch_seg = production_target[batch_step:batch_step + batch_max] # 리스트에서 인덱스로 레인지 범위 분할.
        listed_batch_list.append(batch_seg)

# listed_batch_list = [
#   [(pubs_id, sequence),(pubs_id, sequence), ... ], [(pubs_id, sequence),(pubs_id, sequence), ... ]]

# sqs batch message queue inserting
    for batch in listed_batch_list: # batch = [(pubs_id, sequence),(pubs_id, sequence), ... ]
        entries = list()
        for enu, row in enumerate(batch):
            pubs_id     = row[0]
            seq	        = row[1]
            url         = row[2]

            random_str = utils.string_generator(128)
            message_group_id = random_str[1] if random_str[0] == 0 else None

            message_id 	= 'message_{}'.format(enu)
            seq_num 	= seq if isinstance(seq, str) else str(seq)
            sqs_message = {
                'Id': message_id,
                'MessageBody':str({"jobId": "collect", 
                                   "data": {"pubs_id":pubs_id, "seq": seq_num, "url": url}}),
                'MessageGroupId': message_group_id
            }
            entries.append(sqs_message)


        try:
			# QUEUE에 메지시 등록	
            sqs_object.send_message_batch(QueueUrl = sqs_url, Entries = entries)

            # Query 영역 -> QUEUE에 문제없이 메지시가 등록된 이후에 진행
            queries_to_insert = list()
            update_target_id  = list()
            for message in entries: # message => dictionary!
                message_body    = ast.literal_eval(message['MessageBody'])
                job_id 		    = message_body['jobId']
                job_seq 	    = message_body['data']['seq']
                job_pubs_id     = message_body['data']['pubs_id']

                # 커밋이 완료된 객체를 대상으로 DB에 업데이트 하기.
                # 1. Source 테이블 pubs_sqs_delivered 항목 UPDATE (True)
                # 2. Producer 테이블 항목 INSERT

                job_pubs_id = utils.safe_int(job_pubs_id)
                job_pubs_id = job_pubs_id[1] if job_pubs_id[0]==0 else None

                insert_query = SqsProducer(
                                            pubs_id 			= job_pubs_id, 
                                            sqs_seq 			= job_seq, 
                                            sqs_job_from 		= job_id,
                                            sqs_produced 		= True,
                                            sqs_produced_pid 	= working_pid,
                                            mq_termin_code 		= 'c01'
                                        )
                queries_to_insert.append(insert_query)
                update_target_id.append(job_pubs_id)
 
            # 일괄 업데이트
            #update({AcquiredPublicSource.pubs_sqs_delivered:True}, synchronize_session = False)
            session.query(AcquiredPublicSource).filter(AcquiredPublicSource.pubs_id.in_(update_target_id)).update({
                AcquiredPublicSource.pubs_sqs_delivered:True}, synchronize_session='fetch')

            session.add_all(queries_to_insert)

            # 일괄 커밋
            session.commit()

        except:
            session.rollback()
            raise
            print('except error')
    session.close()
    print('producer process is over')






if __name__=='__main__':
	
	
	# 안전하게 else 처리 꼭!
	# sys.argv=['/home/ubuntu/christopher/main/jobposition/acquired_public_loader.py', 'collect', 'producer', 'now']
	
    job         = sys.argv[1] if sys.argv[1] else None
    position    = sys.argv[2] if sys.argv[2] else None
    time        = sys.argv[3] if sys.argv[3] else None
    working_pid = os.getpid()
    print("collect producer is current running on pid of {}".format(working_pid))

    boto_class = awsutils.AWSBoto3()

    #try:
        
    sqs_set = boto_class.sqs(_queue_type=job)	
    if sqs_set[0]==0:
        sqs_object = sqs_set[1]
        queue_url  = sqs_set[2]
			
    #sqs_batch_message_producing(sqs)
        if job == 'collect':
            sqs_batch_message(sqs_object, queue_url)
				
    #except:
    #    sqs_object, queue_url = (None,None)
    #    print("argument parse failed")
