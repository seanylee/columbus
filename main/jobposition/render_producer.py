import sys, os, ast
import json
import uuid


sys.path.append(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
        )


from jobposition.tools import config, awsutils, utils
from jobposition.tools.models import *

from sqlalchemy.sql.expression import cast
from sqlalchemy import and_, or_
from sqlalchemy import func


def sqs_batch_message(sqs_object, sqs_url):

    batch_max = config.SQS_BATCH_MAX # config의 변수값 호출. 배치 메시지는 10개가 최대!

    # QUERY CONDITION :: "status" like '2%' or '3#', Group by "domain" Having "domain" count to be equal to 1
    pc_targets = [(inst[0], inst[1], inst[2], 'pc') for inst in session.query(
                    func.max(BusinessDataCollectionUrl.burl_id)
                    , func.max(BusinessDataCollectionUrl.burl_scheme)
                    , BusinessDataCollectionUrl.burl_domain
                        ).filter(
                            or_(
                                cast(BusinessDataCollectionUrl.burl_pc_status, sqlalchemy.String).like('3%')
                                , cast(BusinessDataCollectionUrl.burl_pc_status, sqlalchemy.String).like('2%')
                            )
                        ).group_by(
                            BusinessDataCollectionUrl.burl_domain
                        ).having(
                            func.count(BusinessDataCollectionUrl.burl_domain) == 1
                        ).all()
                ]

    mobile_targets = [(inst[0], inst[1], inst[2], 'mobile') for inst in session.query(
                    func.max(BusinessDataCollectionUrl.burl_id)
                    , func.max(BusinessDataCollectionUrl.burl_scheme)
                    , BusinessDataCollectionUrl.burl_domain
                        ).filter(
                            or_(
                                cast(BusinessDataCollectionUrl.burl_mobile_status, sqlalchemy.String).like('3%')
                                , cast(BusinessDataCollectionUrl.burl_mobile_status, sqlalchemy.String).like('2%')
                            )
                        ).group_by(
                            BusinessDataCollectionUrl.burl_domain
                        ).having(
                            func.count(BusinessDataCollectionUrl.burl_domain) == 1
                        ).all()
                ]

    targets = pc_targets + mobile_targets


    # sqs batch group generate
    listed_batch_list = list()

    for batch_step in range(0,len(targets), batch_max):
        batch_seg = targets[batch_step:batch_step + batch_max] # 리스트에서 인덱스로 레인지 범위 분할.
        listed_batch_list.append(batch_seg)


    # batch message registration
    for batch in listed_batch_list: # batch = [(id, scheme, domain),(id, scheme, domain), ... ]
        entries = list()
        for enu, row in enumerate(batch):
            burl_id     = row[0]
            burl_scheme = row[1]
            burl_domain = row[2]
            device_type = row[3]

            random_str = utils.string_generator(128)
            message_group_id = random_str[1] if random_str[0]==0 else None

            message_id  = 'message_{}'.format(enu)
            sqs_message = {
                'Id': message_id,
                'MessageBody':str({"jobId": "render",
                                   "data": {"device":device_type,
                                            "burl_id":burl_id,
                                            "burl_scheme": burl_scheme,
                                            "burl_domain": burl_domain}
                                  }),
                'MessageGroupId': message_group_id
            }
            entries.append(sqs_message)

        try:
            # QUEUE에 메지시 등록
            sqs_object.send_message_batch(QueueUrl = sqs_url, Entries = entries)

            # Query 영역 -> QUEUE에 문제없이 메지시가 등록된 이후에 진행

            queries_to_insert = list()
            update_target_id  = list()
            for message in entries: # message => dictionary!
                message_body        = ast.literal_eval(message['MessageBody'])
                job_id              = message_body['jobId']
                job_burl_id         = message_body['data']['burl_id']
                job_burl_scheme     = message_body['data']['burl_scheme']
                job_burl_domain     = message_body['data']['burl_domain']
                device              = message_body['data']['device']

                # 커밋이 완료된 객체를 대상으로 DB에 업데이트 하기.
                # 1. Source 테이블 pubs_sqs_delivered 항목 UPDATE (True)
                # 2. Producer 테이블 항목 INSERT

                job_burl_id = utils.safe_int(job_burl_id)
                job_burl_id = job_burl_id[1] if job_burl_id[0]==0 else None

                insert_query = SqsProducer(
                                            burl_id             = job_burl_id,
                                            sqs_seq             = None,
                                            sqs_job_from        = job_id,
                                            sqs_produced        = True,
                                            sqs_produced_pid    = working_pid,
                                            mq_termin_code      = 'c01'
                                        )
                queries_to_insert.append(insert_query)
                update_target_id.append(job_burl_id)



            # 일괄 업데이트
            #update({AcquiredPublicSource.pubs_sqs_delivered:True}, synchronize_session = False)
            session.add_all(queries_to_insert)
        except:
            print('except error')




    burl_for_pc         = [each[0] for each in pc_targets]
    burl_for_mobile     = [each[0] for each in mobile_targets]
                
    session.query(BusinessDataCollectionUrl).\
        filter(BusinessDataCollectionUrl.burl_id.in_(burl_for_pc)).\
        update({"burl_pc_render_cnt":BusinessDataCollectionUrl.burl_pc_render_cnt + 1
                }, synchronize_session=False)

    session.query(BusinessDataCollectionUrl).\
        filter(BusinessDataCollectionUrl.burl_id.in_(burl_for_mobile)).\
        update({"burl_mobile_render_cnt":BusinessDataCollectionUrl.burl_mobile_render_cnt + 1
                }, synchronize_session=False)





    try:
        # 일괄 커밋
        session.commit()

    except:
        session.rollback()
        print('except error')
    session.close()
    print('producer process is over')



if __name__=='__main__':


    # 안전하게 else 처리 꼭!
    # sys.argv=['/home/ubuntu/christopher/main/jobposition/acquired_public_loader.py', 'collect', 'producer', 'now']

    job         = sys.argv[1] if sys.argv[1] else None
    position    = sys.argv[2] if sys.argv[2] else None
    time        = sys.argv[3] if sys.argv[3] else None
    working_pid = os.getpid()
    print("collect producer is current running on pid of {}".format(working_pid))

    boto_class = awsutils.AWSBoto3()

    #try:

    sqs_set = boto_class.sqs(_queue_type=job)
    if sqs_set[0]==0:
        sqs_object = sqs_set[1]
        queue_url  = sqs_set[2]

    #sqs_batch_message_producing(sqs)
        if job == 'render':
            sqs_batch_message(sqs_object, queue_url)





