#!/usr/bin/python

import json
import xmltodict
import boto3
import random
import time
import requests
from bs4 import BeautifulSoup
import sys,os
import xml.etree.ElementTree as ET
import datetime
import xmltodict
# Set execution base directory to be main

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from jobposition.tools import utils, config, awsutils
from jobposition.tools.models import *

#from meta.config import SQS


def aworker(_sqs_object, _sqs_url):
    api_key    = random.choice(config.OPENAPI_KEY['data_go_kr'])
    api        = config.OPENAPI_URL['MllInfoService_seq']
    sqs_response = _sqs_object.receive_message(QueueUrl			        = _sqs_url,
                                               AttributeNames			= ['All'],
                                               MessageAttributeNames	= ['string'],
                                               MaxNumberOfMessages		= 1,
                                               WaitTimeSeconds			= 20,)
    # get message obj
    messages = sqs_response.get('Messages', [])

    # delete message
    #_sqs_object.delete_message(QueueUrl=_sqs_url, ReceiptHandle=message.get('ReceiptHandle'))

    for message in messages:
        _sqs_object.delete_message(QueueUrl=_sqs_url, ReceiptHandle=message.get('ReceiptHandle'))

        msg = message.get('Body').replace('\'','"')
        body = json.loads(msg)

        if body.get('jobId', "collect"):
            # what you get : pubs_id, sqs_seq, sqs_job_from, sqs_consumer_pid,
            # bdc_source_type, bdc_business_name, bdc_business_no, 

            body_job_id  = body['jobId']
            body_pubs_id = body['data']['pubs_id']
            body_seq     = body['data']['seq']
            body_url     = body['data']['url']


            # API 발송 정책이 정해지는곳
            if body_url == 'api.data.go.kr/MllInfoService/getMllInfoDetail':
                api_url = body_url + "?seq={}&ServiceKey={}".format(body_seq, api_key)

                r = requests.get(api_url)
                xmlString = r.text
                jsonString = dict(xmltodict.parse(xmlString))
                root = ET.fromstring(xmlString)

                validation = 'valid' if (root.findall('body')[0][0].find('item'))!=None else 'invalid'
                if validation == 'valid':
                    try: # sequence 번호 추출
                        seq = str(root.findall('body')[0][0][0].find('seq').text)
                    except:
                        seq = None
                    try: # 상호명 추출
                        bup_nm = str(root.findall('body')[0][0][0].find('bupNm').text)
                    except:
                        bup_nm = None
                    try: # 사업자명 추출
                        repsnt_nm = str(root.findall('body')[0][0][0].find('repsntNm').text)
                    except:
                        repsnt_nm = None
                    try: # 인터넷도메인
                        dmnNm = str(root.findall('body')[0][0][0].find('dmnNm').text)
                        domain_list = utils.url_extractor(dmnNm)[1] if utils.url_extractor(dmnNm)[0]==0 else []
                        domains = None if len(domain_list)==0 else domain_list
                    except:
                        dmnNm = None
                        domains = None
                    try: # 사업자등록번호
                        wrkrNo = str(root.findall('body')[0][0][0].find('wrkrNo').text)
                    except:
                        wrkrNo = None
                    try: # 신고기관대표연락처
                        rep_telno = str(root.findall('body')[0][0][0].find('repTelno').text)
                        rep_telno = rep_telno.split(',')[0]
                    except:
                        rep_telno = None
                    try: # 신고일자
                        perm_ymd = str(root.findall('body')[0][0][0].find('permYmd').text)
                        #permymd = ''.join(datetime_validator(perm_ymd)[1]) if datetime_validator(perm_ymd)[0]==0 else None
                    except:
                        perm_ymd = None
                    try: # address
                        addr = str(root.findall('body')[0][0][0].find('addr').text)
                    except:
                        addr = None
                    try: # new addr
                        newaddr = str(root.findall('body')[0][0][0].find('newaddr').text)
                    except:
                        newaddr = None

                    # sqs_producer_id 받아오기
                    session.add(
                        SqsConsumer(
                            sqs_producer_id             = None,
                            sqs_seq                     = body_seq,
                            sqs_job_from                = body_job_id,
                            sqs_consumed                = True,
                            sqs_consumed_pid            = working_pid,
                            mq_termin_code              = 'c01'
                        )
                    )

                    session.add(
                        BusinessDataCollection(
                            bdc_source_type         = 'public',
                            bdc_business_name       = bup_nm,
                            bdc_business_no         = wrkrNo,
                            bdc_business_repsnt     = repsnt_nm,
                            bdc_business_addr       = addr,
                            bdc_business_newaddr    = newaddr,
                            bdc_business_tel        = rep_telno,
                            bdc_url_all             = domains,
                            bdc_url_loaded          = False,
                            bdc_established_date    = perm_ymd,
                            bdc_raw_message         = jsonString
                        )
                    )

                elif validation == 'invalid':
                    # sqs_producer & acquired_public_source
                    # row 삭제하기
                    source_del = session.query(AcquiredPublicSource).filter(AcquiredPublicSource.pubs_seq==int(body_seq))
                    session.delete(source_del)

                    log_del    = session.query(SqsProducer).filter(SqsProducer.sqs_seq==int(body_seq))
                    session.delete(log_del)


            elif 'm.kakao.com' in body_url:
                api_url = body_url + "/{}".format(body_seq)

                random_ua   = random.choice(config.MOBILE_AND_UA)
                header      = {'User-Agent': random_ua}
                r = requests.get(api_url, headers=header)

                soup = BeautifulSoup(r.text, 'html.parser')
                page_examine = "not_found" if soup.find("strong", "tit_err") else 'found'
                if page_examine == 'found':
                    data            = soup.find("script", {"id":"initial-data"})['data-json']
                    data_profile    =  json.loads(data)['entities']['profiles']
                    





                elif page_examing == 'not_found':
                    None


           
            try:
                session.commit()
            except:
                session.rollback()
                raise
            finally:
                session.close()
        else:
            print('JobId is not valid!')
            _sqs_object.delete_message(QueueUrl=_sqs_url, ReceiptHandle=message.get('ReceiptHandle'))


        # XML to Json
        #jsonString = dict(xmltodict.parse(xmlString))

        #for instance in session.query(PublicDataList).filter_by(sequence=body_sequence):
        #    instance.request_attempt=True
        #    instance.response_code = response_status
        #    instance.response_body = True

# UPSERT 반드시 구현 ==> seq번호가 Unique 값 이기 때문에 !!!
# existance = session.query(PublicDataDetail).filter_by(sequence=body_sequence)
# print(existance)
# if existance:
#     existance.domain=dmnNm
#     existance.business_no=wrkrNo
#     existance.res_xml=r.text
#     existance.res_json=jsonString
#     existance.item_attr_cnt=item_attrs_cnt
#     session.add(existance)
# elif not existance:





    # DB 입력 두군데-> sqs_consuemr, business data collection



        #session.add(
        #    PublicDataDetail(
        #        sequence		= body_sequence,
        #        domain			= dmnNm,
        #        business_no		= wrkrNo,
        #        res_xml			= r.text,
        #        res_json		= jsonString,
        #        item_attr_cnt	= item_attrs_cnt
        #    )
        #)

        #session.commit()

        #sqs.delete_message(QueueUrl=SQS_QUEUE_URL, ReceiptHandle=message.get('ReceiptHandle'))
        #print(
        #    datetime_indic +
        #    'Consumer process "{}" for the sequence of "{}" is completed! '.format(os.getpid(), str(body_sequence)
        #    +'Message has deleted from the queue')
        #)
        # except Exception as e:
            # print('Exception in worker > ', e)
            # sqs.delete_message(QueueUrl=SQS_QUEUE_URL, ReceiptHandle=message.get('ReceiptHandle'))




if __name__ == '__main__':

    job         = sys.argv[1] if sys.argv[1] else None
    position    = sys.argv[2] if sys.argv[2] else None
    time        = sys.argv[3] if sys.argv[3] else None
    working_pid = os.getpid()
    print("collect consumer is current running on pid of {}".format(working_pid))

    boto_class = awsutils.AWSBoto3()

    #try:

    sqs_set = boto_class.sqs(_queue_type='collect')
    if sqs_set[0]==0:
        sqs_object = sqs_set[1]
        queue_url  = sqs_set[2]
        
        while True:
            try:
                if job == 'collect':
                    aworker(sqs_object, queue_url)
            except:
                msg = 'Consumer-{} "{}" 의 작업이 중단되었습니다.'.format(job, working_pid)
                utils.slack_msg(msg)
    print('WORKER "{}" STOPPED'.format(os.getpid()))
