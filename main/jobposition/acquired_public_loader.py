

import sys, os
# Set execution base directory to be main
sys.path.append(
		os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
		)

from jobposition.tools import models, utils

import requests
import random

class PublicSourceLoader:

	"""
	Sailor Registered for the work "load"
	"""

	def __init__(self):
		None
#		self.date           = datetime.datetime.now().strftime('%Y-%m-%d')
#		self.datetime_val   = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')



	def run_now(self,seq_size,organization,source_type,source_type_detail,source_type_extra,schedule_type):
		"""
		Singlerun ==> Generates sequence number all the way upto 700,000.
		"""

		batch_object = utils.rangebatch_generator(start_seq=1, end_seq=seq_size, batch_size=100000)
		batch_list = batch_object[1] if batch_object[0]==0 else None

                

		if batch_list:
			for batch in batch_list:
				batch_orm = [models.AcquiredPublicSource(
								pubs_provide_organiz    = organization
								, pubs_type             = source_type
                                , pubs_type_detail      = source_type_detail
                                , pubs_type_extra       = source_type_extra
								, pubs_gen_type         = schedule_type
                                , pubs_aggregate_key    = '_'.join([organization,source_type,str(dump_)])
								, pubs_seq              = dump_
                            ) for dump_ in batch]
				models.session.add_all(batch_orm)
				models.session.commit()

if __name__=='__main__':

	# 안전하게 else 처리 꼭!
	# sys.argv = ['/home/ubuntu/christopher/main/jobposition/acquired_public_loader.py', 'load', 'public', 'now']

	# sys.argv[0] = '/home/ubuntu/christopher/main/jobposition/acquired_public_loader.py'
	# sys.argv[1] = 'load'
	# sys.argv[2] = 'public'
	# sys.argv[3] = 'now'


	job 		= sys.argv[1] if sys.argv[1] else None
	position	= sys.argv[2] if sys.argv[2] else None
	time    	= sys.argv[3] if sys.argv[3] else None
	working_pid = os.getpid()
	print("jobinitializer is current running on pid of {}".format(working_pid))

	loader = PublicSourceLoader()

	if position == 'public':
		# PublicSourceLoader class generate
		loader.run_now(seq_size             = 700000,
                       organization         = 'api.data.go.kr',
                       source_type          = 'MllInfoService',
                       source_type_detail   = 'getMllInfoDetail',
                       source_type_extra    = 'seq',
                       schedule_type        = 'now')

#	elif position == 'dailyrun':
#		if actiondetail == 'actiondetail_not_provided': # Yesterday
#			today = datetime.date.today()
#			actiondetail = (today - datetime.timedelta(days = 1)).strftime('%Y%m%d')
#
#
#		# Input 값으로 받을때 날짜 형식 강제로 통일
#		# 대상 : '2019-01-01', '2019.01.01', '2019/01/01'
#		elif '-' in actiondetail:
#			actiondetail = actiondetail.replace('-','')
#		elif '/' in actiondetail:
#			actiondetail = actiondetail.replace('/','')
#		elif '.' in actiondetail:
#			actiondetail = actiondetail.replace('.','')


	else:
		print("Not defined action name '{}'".format(action))

