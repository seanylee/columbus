import sys, os, ast
import json
import uuid
import psycopg2

sys.path.append(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
        )
from jobposition.tools import config, awsutils, utils
from jobposition.tools.models import *



conn = psycopg2.connect("dbname={} user={} host={} password={}".format(
            config.POSTGRESQL['crm']['name'],config.POSTGRESQL['crm']['user'],
            config.POSTGRESQL['crm']['host'],config.POSTGRESQL['crm']['pass'])
        )

sql = """
    SELECT
        crm_companyobject.id 
        , crm_companyobject.company_name
        , crm_companyobject.address
        , crm_companyobject.website_url
        , crm_companyobject.business_number
     FROM public.crm_companyobject
     WHERE 
        crm_companyobject.website_url IS NOT NULL
"""
bb= """
        AND crm_companyobject.created_at > current_date - interval '3' day
"""



cc= """
try:
    with conn.cursor() as cur:
        cur.execute(sql)
        for fet in cur.fetchall():
            crm_id          = str(fet[0]) if fet[0] else None
            crm_company     = str(fet[1]) if fet[1] else None
            crm_address     = str(fet[2]) if fet[2] else None
            crm_website     = str(fet[3]) if fet[3] else None
            crm_business_no = int(fet[4]) if fet[4] else None
            crm_agg_key     = 'crm_company_object_' + crm_id

            try:
                session.add(
                    AcquiredPrivateSource(
                        pris_aggregate_key          = crm_agg_key,
                        pris_collection_delivered   = False,
                        pris_provide_organiz        = 'crm',
                        pris_type                   = 'crm_company_boject',
                        pris_type_detail            = crm_id,
                        pris_business_name          = crm_company,
                        pris_business_num           = crm_business_no,
                        pris_domain                 = crm_website,
                        pris_address                = crm_address
                    )
                )
                session.flush()
            except:
                session.rollback()
                raise

            session.refresh(AcquiredPrivateSource)
            pris_id_val = AcquiredPrivateSource.id


            try:
                session.add(
                    BusinessDataCollection(
                        pris_id                 = pris_id_val,
                        bdc_source_type         = 'private',
                        bdc_business_name       = crm_company,
                        bdc_business_no         = crm_business_no,
                        bdc_business_addr       = crm_address,
                        bdc_url_all             = [crm_website],
                        bdc_url_loaded          = False,
                    )

                )
                
                session.commit()
            except:
                session.rollback()
                raise

        session.close()
except:
    print(sql)

"""





with conn.cursor() as cur:
    cur.execute(sql)
    for fet in cur.fetchall():
        crm_id          = str(fet[0]) if fet[0] else None
        crm_company     = str(fet[1]) if fet[1] else None
        crm_address     = str(fet[2]) if fet[2] else None
        crm_website     = str(fet[3]) if fet[3] else None
        crm_business_no = int(fet[4]) if fet[4] else None
        crm_agg_key     = 'crm_company_object_' + crm_id

        try:
            session.add(
                AcquiredPrivateSource(
                    pris_aggregate_key          = crm_agg_key,
                    pris_collection_delivered   = False,
                    pris_provide_organiz        = 'crm',
                    pris_type                   = 'crm_company_boject',
                    pris_type_detail            = crm_id,
                    pris_business_name          = crm_company,
                    pris_business_num           = crm_business_no,
                    pris_domain                 = crm_website,
                    pris_address                = crm_address
                )
            )
            session.commit()
        except:
            session.rollback()

session.close()


a='''
            session.flush()

        #session.refresh(AcquiredPrivateSource)
            pris_id_val = AcquiredPrivateSource.id

            session.add(
                BusinessDataCollection(
                    pris_id                 = pris_id_val,
                    bdc_source_type         = 'private',
                    bdc_business_name       = crm_company,
                    bdc_business_no         = crm_business_no,
                    bdc_business_addr       = crm_address,
                    bdc_url_all             = [crm_website],
                    bdc_url_loaded          = False,
                )

            )'''
