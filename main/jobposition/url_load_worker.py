import sys
import os
import ast
import requests
import json
import uuid
from urllib.parse import urlparse
from sqlalchemy import and_
sys.path.append(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
        )

import time
from jobposition.tools import config, awsutils, utils
from jobposition.tools.models import *



def business_data_url_loader():


    for row in session.query(BusinessDataCollection).filter(
                and_(
                    BusinessDataCollection.bdc_url_all != None,
                    BusinessDataCollection.bdc_url_loaded == False
                    )
                ):
        _bdc_id      = row.bdc_id
        _url_all     = row.bdc_url_all


        update_target_id = list()
        update_target_id.append(_bdc_id)
        for each_url in _url_all:
            if each_url.startswith('http')==False:
                each_url = 'http://' + each_url

            purl        = urlparse(each_url)
            scheme      = purl.scheme
            domain      = purl.netloc
            path        = purl.path
            parameter   = purl.params
            query       = purl.query


            target_url      = scheme + '://' + domain
            pc_status       = utils.url_status(_urlstring=target_url, _device='pc')[1]
            mobile_status   = utils.url_status(_urlstring=target_url, _device='mobile')[1]

            session.add(
                BusinessDataCollectionUrl(
                        bdc_id                  = _bdc_id,
                        burl_pc_status          = pc_status,
                        burl_pc_render_cnt      = 0,
                        burl_mobile_status      = mobile_status,
                        burl_mobile_render_cnt  = 0,
                        burl_raw                = each_url,
                        burl_scheme             = scheme,
                        burl_domain             = domain,
                        burl_path               = path,
                        burl_parameter          = parameter,
                        burl_query              = query
                    )
                )


        session.query(BusinessDataCollection).filter(BusinessDataCollection.bdc_id.in_(update_target_id)).update({
            BusinessDataCollection.bdc_url_loaded:True}, synchronize_session='fetch')

        try:
            session.commit()
        except:
            session.rollback()
            raise

    session.close()
    return True






if __name__ == "__main__":

    while True:
        if business_data_url_loader():
            time.sleep(120)
        else:
            break
                           
